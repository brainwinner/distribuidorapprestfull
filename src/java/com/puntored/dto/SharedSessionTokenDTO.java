package com.puntored.dto;

/**
 *
 * @author Juan Rey (Holding Digital)
 */
public class SharedSessionTokenDTO {
    private String token = null;
    
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    
    
}
