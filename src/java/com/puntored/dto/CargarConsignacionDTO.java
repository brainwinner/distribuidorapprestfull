package com.puntored.dto;

public class CargarConsignacionDTO extends DistribuidorDTO{
    
    private String fechaConsignacion;
    private String idBanco;
    private String idDepartamento;
    private String idCiudad;
    private String sucursal;
    private String valor;
    private String imagen;
    private String numeroConsignacion;
    private String idDistribuidor;

    public String getIdDistribuidor() {
        return idDistribuidor;
    }

    public void setIdDistribuidor(String idDistribuidor) {
        this.idDistribuidor = idDistribuidor;
    }

    public String getFechaConsignacion() {
        return fechaConsignacion;
    }

    public void setFechaConsignacion(String fechaConsignacion) {
        this.fechaConsignacion = fechaConsignacion;
    }

    public String getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(String idBanco) {
        this.idBanco = idBanco;
    }

    public String getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(String idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public String getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(String idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String idSucursal) {
        this.sucursal = idSucursal;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getNumeroConsignacion() {
        return numeroConsignacion;
    }

    public void setNumeroConsignacion(String numeroConsignacion) {
        this.numeroConsignacion = numeroConsignacion;
    }
            
    
    
}
