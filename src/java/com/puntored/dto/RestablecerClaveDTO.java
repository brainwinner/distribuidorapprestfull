
package com.puntored.dto;

public class RestablecerClaveDTO extends DistribuidorDTO{
    
    private String idDistribuidor;

    public String getIdDistribuidor() {
        return idDistribuidor;
    }

    public void setIdDistribuidor(String idDistribuidor) {
        this.idDistribuidor = idDistribuidor;
    }
    
}
