
package com.puntored.dto;


public class SolicitarCambioCorteFDTO extends DistribuidorDTO{
    
    private String idComercio;
    private String idCorte;

    public String getIdComercio() {
        return idComercio;
    }

    public void setIdComercio(String idComercio) {
        this.idComercio = idComercio;
    }

    

    public String getIdCorte() {
        return idCorte;
    }

    public void setIdCorte(String idCorte) {
        this.idCorte = idCorte;
    }
    
    
    
}
