
package com.puntored.dto;

public class PasaCupoDTO extends DistribuidorDTO{
    
    private String idComercio;
    private String monto;

    public String getIdComercio() {
        return idComercio;
    }

    public void setIdComercio(String idComercio) {
        this.idComercio = idComercio;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }
    
}
