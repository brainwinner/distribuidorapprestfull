
package com.puntored.dto;


public class AESDTO {
    private String data_1;
    private String data_2;

    public String getData_1() {
        return data_1;
    }

    public void setData_1(String data_1) {
        this.data_1 = data_1;
    }

    public String getData_2() {
        return data_2;
    }

    public void setData_2(String data_2) {
        this.data_2 = data_2;
    }
    
    
}
