
package com.puntored.dto;

public class AbonoTradicionalODTO extends DistribuidorDTO{
    
    private String idComercio;
    private String valor;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getIdComercio() {
        return idComercio;
    }

    public void setIdComercio(String idComercio) {
        this.idComercio = idComercio;
    }
    
}
