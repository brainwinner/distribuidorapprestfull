package com.puntored.dto;

/**
 *
 * @author Holding Digital
 */
public class LoginTerminalDTO {
    private String terminalId;
    private String clave;
  
    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave.toLowerCase();
    }
}
