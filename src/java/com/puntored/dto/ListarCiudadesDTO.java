
package com.puntored.dto;

public class ListarCiudadesDTO extends DistribuidorDTO {
    
    private String idDepartamento;

    public String getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(String idDepartamento) {
        this.idDepartamento = idDepartamento;
    }
}
