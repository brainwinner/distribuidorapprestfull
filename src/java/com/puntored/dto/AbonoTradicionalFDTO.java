
package com.puntored.dto;


public class AbonoTradicionalFDTO extends DistribuidorDTO{
    private String idComercio;

    public String getIdComercio() {
        return idComercio;
    }

    public void setIdComercio(String idComercio) {
        this.idComercio = idComercio;
    }
}
