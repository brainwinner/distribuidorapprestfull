
package com.puntored.dto;


public class ListarCausalNoPagoDTO extends DistribuidorDTO{
    private String tipo;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
