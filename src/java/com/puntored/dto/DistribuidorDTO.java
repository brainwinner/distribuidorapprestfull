package com.puntored.dto;

import com.puntored.util.ValidarCampos;

public class DistribuidorDTO {

    private String cedula;
    private String clave;


    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String contrasena) {
        this.clave = contrasena.toLowerCase();
    }


}
