
package com.puntored.dto;


public class DesbloquearComercioDTO extends DistribuidorDTO{
    
    private String idComercio;

    public String getIdComercio() {
        return idComercio;
    }

    public void setIdComercio(String idComercio) {
        this.idComercio = idComercio;
    }
}
