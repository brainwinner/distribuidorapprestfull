
package com.puntored.dto;



import org.codehaus.jackson.annotate.JsonIgnoreProperties;


 @JsonIgnoreProperties(ignoreUnknown = true)
public class DepositImageDTO {
    
    private String name;
    private String response;
    private boolean state;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the response
     */
    public String getResponse() {
        return response;
    }

    /**
     * @param response the response to set
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * @return the state
     */
    public boolean isState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(boolean state) {
        this.state = state;
    }
    
}
