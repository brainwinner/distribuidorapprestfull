
package com.puntored.dto;


public class BloquearComercioDTO extends DistribuidorDTO{
   
    private String idComercio;
    private String idCausal;

    public String getIdComercio() {
        return idComercio;
    }

    public void setIdComercio(String idComercio) {
        this.idComercio = idComercio;
    }

    public String getIdCausal() {
        return idCausal;
    }

    public void setIdCausal(String idCausal) {
        this.idCausal = idCausal;
    }

   
    
}
