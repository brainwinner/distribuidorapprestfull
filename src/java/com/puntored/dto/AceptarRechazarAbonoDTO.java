
package com.puntored.dto;


public class AceptarRechazarAbonoDTO extends DistribuidorDTO{

    private String idAbono;
    
    public String getIdAbono() {
        return idAbono;
    }

    public void setIdAbono(String idAbono) {
        this.idAbono = idAbono;
    }
  

    
}
