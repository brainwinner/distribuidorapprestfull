/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puntored.dto;

/**
 *
 * @author JCruz
 */
public class ListarCiudadesDepartamentoDTO extends DistribuidorDTO {
    
    private String idDepartamento;

    public String getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(String idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    
    
}
