/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puntored.dto;

/**
 *
 * @author JCruz
 */
public class ListarDepartamentosRegionalDTO extends DistribuidorDTO {
    
    private String idRegional;

    public String getIdRegional() {
        return idRegional;
    }

    public void setIdRegional(String idRegional) {
        this.idRegional = idRegional;
    }
    
    
}
