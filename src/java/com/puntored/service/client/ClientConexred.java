/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puntored.service.client;

import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.channel.ASCIIChannel;
import org.jpos.iso.packager.ISO87APackager;
import org.jpos.util.Logger;
import org.jpos.util.SimpleLogListener;

/**
 *
 * @author fharias
 */
public class ClientConexred {

    private ASCIIChannel channel = null;
    private String host = null;
    private int port = 0;
    private int timeout = 0;
    private static PrintStream out = null;
    private static String fecha = null;
    private boolean debug;

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public ClientConexred(String host, int port) {
        this.host = host;
        this.port = port;
        this.timeout = 60000;
        getLog();
    }

    private static void getLog() {
        SimpleDateFormat sdf = null;
        sdf = new SimpleDateFormat("yyyyMMdd_HHmm");
        out = System.out;
    }

    public boolean connect() {
        boolean estado = false;
        channel = new ASCIIChannel(host, port, new ISO87APackager());
        try {
            Logger logger = new Logger();
            logger.addListener(new SimpleLogListener(out));
            if (debug) channel.setLogger(logger, "CXR-CLIENT");
            channel.setTimeout(timeout);
            channel.connect();
            estado = channel.isConnected();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return estado;
    }

    public ISOMsg enviar(ISOMsg message) throws IOException, ISOException {
        ISOMsg response = null;
        channel.send(message);
        response = channel.receive();
        channel.disconnect();
        return response;
    }
}
