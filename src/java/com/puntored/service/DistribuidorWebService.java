
package com.puntored.service;

import com.puntored.dto.AESDTO;
import com.puntored.dto.AbonoTradicionalFDTO;
import com.puntored.dto.AbonoTradicionalODTO;
import com.puntored.dto.AceptarRechazarAbonoDTO;
import com.puntored.dto.ActualizarPosicionDTO;
import com.puntored.dto.AfiliacionDTO;
import com.puntored.dto.BloquearComercioDTO;
import com.puntored.dto.CargarConsignacionDTO;
import com.puntored.dto.ConsultarSaldoOnlineDTO;
import com.puntored.dto.DesbloquearComercioDTO;
import com.puntored.process.SolicitudRestablecerContrasena;
import com.puntored.process.RestablecerContrasenaProcess;
import com.puntored.process.Login;
import com.puntored.dto.ListarCiudadesDTO;
import com.puntored.dto.DistribuidorDTO;
import com.puntored.dto.EditarMargenProductoDTO;
import com.puntored.dto.ListarAbonosDTO;
import com.puntored.dto.ListarCausalNoPagoDTO;
import com.puntored.dto.ListarCiudadesDepartamentoDTO;
import com.puntored.dto.ListarDepartamentosRegionalDTO;
import com.puntored.dto.PasaCupoDTO;
import com.puntored.dto.RestablecerClaveDTO;
import com.puntored.dto.SolicitarCambioCorteFDTO;
import com.puntored.dto.VerComercioDTO;
import com.puntored.process.AbonoTradicionalFacturaProcess;
import com.puntored.process.AbonoTradicionalOnlineProcess;
import com.puntored.process.AceptarAbonoProcess;
import com.puntored.process.ActualizarPosicionProcess;
import com.puntored.process.AfiliacionProcess;
import com.puntored.process.BloquearComercioProcess;
import com.puntored.process.CargarConsignacionProcess;
import com.puntored.process.ConsultarMargenProductosProcess;
import com.puntored.process.ConsultarPlanillaMargenProcess;
import com.puntored.process.ConsultarSaldoOnlineProcess;
import com.puntored.process.DesbloquearComercioProcess;
import com.puntored.process.EditarMargenProductosProcess;
import com.puntored.process.InfoDistribuidorProcess;
import com.puntored.process.ListarAbonos;
import com.puntored.process.ListarBancosProcess;
import com.puntored.process.ListarCausalNoPagoProcess;
import com.puntored.process.ListarCiudadesDepartamentoProcess;
import com.puntored.process.ListarCiudadesProcess;
import com.puntored.process.ListarComerciosProcess;
import com.puntored.process.ListarConsignacionesProcess;
import com.puntored.process.ListarCortesFacturProcess;
import com.puntored.process.ListarCuposProcess;
import com.puntored.process.ListarDepartamentosProcess;
import com.puntored.process.ListarDepartamentosRegionalProcess;
import com.puntored.process.ListarRegionalesProcess;
import com.puntored.process.ListarTiposEstablecimientoProcess;
import com.puntored.process.PasarCupoProcess;
import com.puntored.process.RechazarAbonoProcess;
import com.puntored.process.SolicitarCambioCorteFProcess;
import com.puntored.process.VerComercioProcess;
import com.puntored.util.AES;
import com.puntored.util.ResponseApi;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author JCruz
 */
@Path("/Distribuidor")
public class DistribuidorWebService {
    

    @Context
    HttpServletRequest request;

    @POST
    @Path("/Login")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi login(AESDTO request) {
        System.out.println("Ingreso a Login......");
        
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        
        try {
            DistribuidorDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), DistribuidorDTO.class);
            responseApi = new Login().makeLogin(dTO);
        } catch (Exception e) {
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return responseApi;
    }

    @POST
    @Path("/SolicitudNuevaClave")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi solicitarNuevaClave(AESDTO request) {
        System.out.println("Ingreso a solicitar nueva clave");
        
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            DistribuidorDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), DistribuidorDTO.class);
            responseApi = new SolicitudRestablecerContrasena().solicitarNuevoPassword(dTO);
        } catch (Exception e) {
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());   
            e.printStackTrace();
        }
        return responseApi;
    }

    @POST
    @Path("/RestablecerClave")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi RestablecerClave(AESDTO request) {
        System.out.println("Ingreso a restablecer clave");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            RestablecerClaveDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), RestablecerClaveDTO.class);
            responseApi = new RestablecerContrasenaProcess().restablecerClave(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
            e.printStackTrace();
        }

        return responseApi;
    }

    @POST
    @Path("/InformacionDistribuidor")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi informacionDistribuidor(AESDTO request) {
        System.out.println("Ingreso a consultar informacion");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            DistribuidorDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), DistribuidorDTO.class);
            responseApi = new InfoDistribuidorProcess().getInfoDistribuidor(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        
        return responseApi;
    }

    @POST
    @Path("/ListarComercios")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi listarComercios(AESDTO request) {
        System.out.println("Ingreso a listar comercios");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            DistribuidorDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), DistribuidorDTO.class);
            responseApi = new ListarComerciosProcess().getComercios(dTO);
        } catch (Exception e) {
            e.printStackTrace();;
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        
        return responseApi;
    }
    
    @POST
    @Path("/ListarBancos")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi listarBancos (AESDTO request) {
        System.out.println("Ingreso a Listar Bancos...");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            DistribuidorDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), DistribuidorDTO.class);
            responseApi = new ListarBancosProcess().getBancos(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    @POST
    @Path("/ListarDepartamentos")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi listarDepartamentos (AESDTO request) {
        System.out.println("Ingreso a Listar Departamentos...");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            DistribuidorDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), DistribuidorDTO.class);
            responseApi = new ListarDepartamentosProcess().getDepartamentos(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    @POST
    @Path("/ListarCiudades")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi listarCiudades (AESDTO request) {
        System.out.println("Ingreso a Listar Ciudades...");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        
        try {
            ListarCiudadesDTO lcrDTO = null;
            lcrDTO = mapper.readValue(AES.getDataFromAESRequest(request), ListarCiudadesDTO.class);
            responseApi = new ListarCiudadesProcess().getCiudades(lcrDTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    @POST
    @Path("/ListarAbonos")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi listarAbonos (AESDTO request) {
        System.out.println("Ingreso a Listar Ciudades...");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        
        try {
            ListarAbonosDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), ListarAbonosDTO.class);
            responseApi = new ListarAbonos().getAbonos(dTO);
            
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    @POST
    @Path("/ListarConsignaciones")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi listarConsignaciones (AESDTO request) {
        System.out.println("Ingreso a Listar Consignaciones...");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            DistribuidorDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), DistribuidorDTO.class);
            responseApi = new ListarConsignacionesProcess().getConsignaciones(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    
    
    
    
    @POST
    @Path("/ListarCupos")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi listarCupos (AESDTO request) {
        System.out.println("Ingreso a Listar Cupos...");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            DistribuidorDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), DistribuidorDTO.class);
            responseApi = new ListarCuposProcess().getCupos(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    @POST
    @Path("/CargarConsignacion")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi cargarConsignacion (AESDTO request) {
        System.out.println("Ingreso a cargar consignacion...");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            CargarConsignacionDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), CargarConsignacionDTO.class);
            responseApi = new CargarConsignacionProcess().cargarConsignacion(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    @POST
    @Path("/AceptarAbono")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi aceptarAbono (AESDTO request) {
        System.out.println("Ingreso a Aceptar abono...");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            AceptarRechazarAbonoDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), AceptarRechazarAbonoDTO.class);
            responseApi = new AceptarAbonoProcess().aceptarAbono(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    
    
    @POST
    @Path("/ListarCortesFacturacion")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi listarCortesFacturacion (AESDTO request) {
        System.out.println("Ingreso a listarCortesFacturacion...");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            DistribuidorDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), DistribuidorDTO.class);
            responseApi = new ListarCortesFacturProcess().getCortesFactr(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    @POST
    @Path("/ListarCausalesNoPago")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi listarCausalesNoPago (AESDTO request) {
        System.out.println("Ingreso a listarCausalesNoPago...");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            ListarCausalNoPagoDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), ListarCausalNoPagoDTO.class);
            responseApi = new ListarCausalNoPagoProcess().getCausalNoPago(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    @POST
    @Path("/AbonoTradicionalFactura")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi abonoTradicionalFactura (AESDTO request) {
        System.out.println("Ingreso a abonoTradicionalFactura ..");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            AbonoTradicionalFDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), AbonoTradicionalFDTO.class);
            responseApi = new AbonoTradicionalFacturaProcess().getAbonoTradicional(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    @POST
    @Path("/AbonoTradicionalOnline")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi abonoTradicionalOnline (AESDTO request) {
        System.out.println("Ingreso a abonoTradicionalOnline...");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            AbonoTradicionalODTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), AbonoTradicionalODTO.class);
            responseApi = new AbonoTradicionalOnlineProcess().getAbonoTradicionalO(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    
    @POST
    @Path("/SolicitarCambioCorteFacturacion")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi solicitarCambioCorteFacturacion (AESDTO request) {
        System.out.println("Ingreso a solicitarCambioCorteFacturacion...");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            SolicitarCambioCorteFDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), SolicitarCambioCorteFDTO.class);
            responseApi = new SolicitarCambioCorteFProcess().getSolicitarCamabioCorteF(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    @POST
    @Path("/BloquearComercio")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi bloquearComercio (AESDTO request) {
        System.out.println("Ingreso a bloquearComercio..");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            BloquearComercioDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), BloquearComercioDTO.class);
            responseApi = new BloquearComercioProcess().getBloquearComercio(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    
    @POST
    @Path("/DesbloquearComercio")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi desbloquearComercio (AESDTO request) {
        System.out.println("Ingreso a bloquearComercio..");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            DesbloquearComercioDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), DesbloquearComercioDTO.class);
            responseApi = new DesbloquearComercioProcess().getDesbloquearComercio(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    
    @POST
    @Path("/VerComercio")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi verComercio (AESDTO request) {
        System.out.println("Ingreso a bloquearComercio..");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            VerComercioDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), VerComercioDTO.class);
            responseApi = new VerComercioProcess().getComercio(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    @POST
    @Path("/ConsultarSaldoOnline")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi consultarSaldoOnline (AESDTO request) {
        System.out.println("Ingreso a bloquearComercio..");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            ConsultarSaldoOnlineDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), ConsultarSaldoOnlineDTO.class);
            responseApi = new ConsultarSaldoOnlineProcess().getConsultarSaldoOnline(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    @POST
    @Path("/RechazarAbono")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi rechazarAbono (AESDTO request){
        System.out.println("Ingreso a rechazarAbono.... ");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        
        try{
            AceptarRechazarAbonoDTO dto = null;
            dto = mapper.readValue(AES.getDataFromAESRequest(request),AceptarRechazarAbonoDTO.class);
            responseApi =  new RechazarAbonoProcess().rechazarAbono(dto);
            
        }catch(Exception e){
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    @POST
    @Path ("/PasarCupo")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({ MediaType.APPLICATION_JSON})
    public ResponseApi pasarCupo(AESDTO request){
        System.out.println("Ingreso a Pasar Cupo.... ");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi  responseApi = null;
        
        try{
            PasaCupoDTO dto = null;
            dto = mapper.readValue(AES.getDataFromAESRequest(request), PasaCupoDTO.class);
            responseApi = new PasarCupoProcess().pasarCupo(dto);
        
        }
        catch(Exception e){
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
    
    
    return responseApi;
    }
    
    @POST
    @Path("/ListarRegionales")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi listarRegionales(AESDTO request) {
        System.out.println("Ingreso a listar regionales");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            DistribuidorDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), DistribuidorDTO.class);
            responseApi = new ListarRegionalesProcess().getRegionales(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    
    @POST
    @Path("/ListarDepartamentosRegional")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi listarDepartamentosRegional(AESDTO request) {
        System.out.println("Ingreso a listar departamentos por regional");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            ListarDepartamentosRegionalDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), ListarDepartamentosRegionalDTO.class);
            responseApi = new ListarDepartamentosRegionalProcess().getDepartamentosRegional(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    
    @POST
    @Path("/ListarCiudadesDepartamento")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi listarCiudadesDepartamento(AESDTO request) {
        System.out.println("Ingreso a listar departamentos por regional");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            ListarCiudadesDepartamentoDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), ListarCiudadesDepartamentoDTO.class);
            responseApi = new ListarCiudadesDepartamentoProcess().getCiudadesDepartamentos(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
     @POST
    @Path("/ListarTiposEstablecimiento")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi listarTiposEstablecimiento(AESDTO request) {
        System.out.println("Ingreso a listar regionales");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            DistribuidorDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), DistribuidorDTO.class);
            responseApi = new ListarTiposEstablecimientoProcess().getTiposEstablecimiento(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    @POST
    @Path("/Afiliacion")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi afiliarAliado(AESDTO request) {
        System.out.println("Ingreso a afiliar aliado");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            AfiliacionDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), AfiliacionDTO.class);
            responseApi = new AfiliacionProcess().makeAfiliacion(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
 
    
    @POST
    @Path("/ConsultarMargenProductos")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi ConsultarMargenProductos(AESDTO request) {
        System.out.println("Ingreso a consultar margen productos");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            VerComercioDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), VerComercioDTO.class);
            responseApi = new ConsultarMargenProductosProcess().getComercioMargen(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    
    @POST
    @Path("/ConsultarPlanillaMargenes")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi ConsultarPlanillaMargenes(AESDTO request) {
        System.out.println("Ingreso a consultar planilla margenes");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            DistribuidorDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), DistribuidorDTO.class);
            responseApi = new ConsultarPlanillaMargenProcess().getMargenes(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    
    @POST
    @Path("/EditarMargenProductos")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi editarMargenProductos(AESDTO request) {
        System.out.println("Ingreso a editar margenes productos");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        try {
            EditarMargenProductoDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), EditarMargenProductoDTO.class);
            responseApi = new EditarMargenProductosProcess().editMargenProducto(dTO);
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        return responseApi;
    }
    
    @POST
    @Path("/ActualizarPosicion")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi actualizarPosicion (AESDTO request) {
        System.out.println("Ingreso a actualizar posicion");
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi = null;
        
        try {
            ActualizarPosicionDTO dTO = null;
            dTO = mapper.readValue(AES.getDataFromAESRequest(request), ActualizarPosicionDTO.class);
            responseApi = new ActualizarPosicionProcess().getActualizarPosicion(dTO); 
        } catch (Exception e) {
            e.printStackTrace();
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
        }
        
        return responseApi;
        
    }
    
    
    
    public static void main (String[] arg) {
       
        System.out.println("ejecuntado...");
        try{
            AESDTO aesdto = new AESDTO();
            aesdto.setData_1("0123456789123");
            aesdto.setData_2("YiPRBCSo+5lF+vTdBmw8zEkJh53LiopTd6ulw9BkgTpYy1685bWpiDiEx7W9dQgw+WWlVG4p/GvUW/xwo63xRyylHYeKz2Gbea9vWZ8Ed2xvE0YqKvsBctIfZ9Fsu8ZCzmlGeOWRE9IujXmhBy3VJJy3OnU9HL5SRJy63XZJRsqlDnRtCiq8E4w1XjXJ6J8BC220BtjMupHKvqEEdVgZl16lh/ma2bzsbqwAB/hNtSEM4IlnsDsZehzSTsyLqQqeC+FWDU8U4t+rr7T870nDCMjzkGqmGwVvgB/ZfeM/vnwwTGPeq5k8j8aYTBslUrFN9/Rd/45IqEC1n/iSpGDckL3CvAIKxrGqNrwZpxQ8ZQesSLfIT5kH1e+L5n8aR/FWZokVuSr2p+MQM32mM9fZJHL2Q2a3OLnLwO+uIO9YYkmYTWh4r0GY/GZcoV2s+a59xcgTPde7kOXKOh+uJlkbyeaIXBbJXH+PjuW5jt22Vr333HF+yOO0irexjHQi1wjpKbr3EzC8sfGGGCWioe9AKJglag9LLXbdfplcc2+GZkHgTBJLGL7tfPQ2p40FDF59/KGIGVF4yR2pHiO8nBTYjMS/o+fNs246fdedWXw+SGv6I1iYnX74H80axlTaIEDuuKyBIHni6XQjLpOwoTXT+RwevHyTvTyxTTJ+XOd79neXUKAbf/zF0UWBpjNPKhRMjQ2Oa4omci6imo7HckByW8seHU7nzrYN71PnUxg/vYMsIIxxabek0r3Gvd/rowDskdDRmxDdiriqnniKhqKJxG1oiAwT57zcqcVHKkYbo+X9EeiQBb7LsZ2U20bV046odUzfRU3TCMcZwWn4wUNPBspsiMRqo+qZ9l8+grtm54bRSNLxo2Wg1ShUnRo7lQ9TCMaXhto4OPhtJtZotJwHvGEkd8V7N9Qh7Awjhr0LGWc44iuiLf3dgGc6AUOr22Ccs4Iudsn8so2Sk7Jo6zRm+O9xMCZ+nwU+gWIFH999IxhCYz2BQsvt8aiPWRujd9VCIRrPq6XTOhwqCvhMxPMiX6uDbWvdZ5KlJL9xTwWw2jksOriRa81t/fvNlW19N69IavN3tberqhszVf6ncdb6N8whpZDP0XunyfjfeysK+/PgotkjlCGPxVXMAIoxdm6XppYHKAreENKGYbrtdC6KMA==");
            
            System.out.println(AES.getDataFromAESRequest(aesdto));
            
        } catch (Exception e) {
            e.printStackTrace();;
        }
    }
    
    
    

}
