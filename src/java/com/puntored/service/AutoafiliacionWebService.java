
package com.puntored.service;

import com.puntored.dto.AESDTO;
import com.puntored.dto.LoginTerminalDTO;
import com.puntored.dto.SharedSessionTokenDTO;
import com.puntored.process.LoginTerminalProcess;
import com.puntored.process.ValidateSharedSessionTokenProcess;
import com.puntored.util.AES;
import com.puntored.util.ResponseApi;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author Holding Digital
 */
@Path("/Autoafiliacion")
public class AutoafiliacionWebService {
    

    @Context
    HttpServletRequest request;

    @POST
    @Path("/Login")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi login(AESDTO request) {
        System.out.println("Ingreso a Login......");
        
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi;
        
        try {
            LoginTerminalDTO dto = mapper.readValue(AES.getDataFromAESRequest(request), LoginTerminalDTO.class);
            responseApi = new LoginTerminalProcess().login(dto);
        } catch (Exception e) {
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return responseApi;
    }

    @POST
    @Path("/ValidarToken")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseApi validateSharedSessionToken(AESDTO request) {
        System.out.println("Ingreso a validar token de sesión compartida......");
        
        ObjectMapper mapper = new ObjectMapper();
        ResponseApi responseApi;
        
        try {
            SharedSessionTokenDTO dto = mapper.readValue(AES.getDataFromAESRequest(request), SharedSessionTokenDTO.class);
            responseApi = new ValidateSharedSessionTokenProcess().validateToken(dto);
        } catch (Exception e) {
            responseApi = new ResponseApi();
            responseApi.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return responseApi;
    }
    
    public static void main (String[] arg) {
        System.out.println("ejecuntado...");
        try{
            AESDTO aesdto = new AESDTO();
            aesdto.setData_1("0123456789123");
            aesdto.setData_2("YiPRBCSo+5lF+vTdBmw8zEkJh53LiopTd6ulw9BkgTpYy1685bWpiDiEx7W9dQgw+WWlVG4p/GvUW/xwo63xRyylHYeKz2Gbea9vWZ8Ed2xvE0YqKvsBctIfZ9Fsu8ZCzmlGeOWRE9IujXmhBy3VJJy3OnU9HL5SRJy63XZJRsqlDnRtCiq8E4w1XjXJ6J8BC220BtjMupHKvqEEdVgZl16lh/ma2bzsbqwAB/hNtSEM4IlnsDsZehzSTsyLqQqeC+FWDU8U4t+rr7T870nDCMjzkGqmGwVvgB/ZfeM/vnwwTGPeq5k8j8aYTBslUrFN9/Rd/45IqEC1n/iSpGDckL3CvAIKxrGqNrwZpxQ8ZQesSLfIT5kH1e+L5n8aR/FWZokVuSr2p+MQM32mM9fZJHL2Q2a3OLnLwO+uIO9YYkmYTWh4r0GY/GZcoV2s+a59xcgTPde7kOXKOh+uJlkbyeaIXBbJXH+PjuW5jt22Vr333HF+yOO0irexjHQi1wjpKbr3EzC8sfGGGCWioe9AKJglag9LLXbdfplcc2+GZkHgTBJLGL7tfPQ2p40FDF59/KGIGVF4yR2pHiO8nBTYjMS/o+fNs246fdedWXw+SGv6I1iYnX74H80axlTaIEDuuKyBIHni6XQjLpOwoTXT+RwevHyTvTyxTTJ+XOd79neXUKAbf/zF0UWBpjNPKhRMjQ2Oa4omci6imo7HckByW8seHU7nzrYN71PnUxg/vYMsIIxxabek0r3Gvd/rowDskdDRmxDdiriqnniKhqKJxG1oiAwT57zcqcVHKkYbo+X9EeiQBb7LsZ2U20bV046odUzfRU3TCMcZwWn4wUNPBspsiMRqo+qZ9l8+grtm54bRSNLxo2Wg1ShUnRo7lQ9TCMaXhto4OPhtJtZotJwHvGEkd8V7N9Qh7Awjhr0LGWc44iuiLf3dgGc6AUOr22Ccs4Iudsn8so2Sk7Jo6zRm+O9xMCZ+nwU+gWIFH999IxhCYz2BQsvt8aiPWRujd9VCIRrPq6XTOhwqCvhMxPMiX6uDbWvdZ5KlJL9xTwWw2jksOriRa81t/fvNlW19N69IavN3tberqhszVf6ncdb6N8whpZDP0XunyfjfeysK+/PgotkjlCGPxVXMAIoxdm6XppYHKAreENKGYbrtdC6KMA==");
            
            System.out.println(AES.getDataFromAESRequest(aesdto));
            
        } catch (Exception e) {
            e.printStackTrace();;
        }
    }
}
