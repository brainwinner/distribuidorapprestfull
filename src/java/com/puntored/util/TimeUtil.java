/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puntored.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimeUtil {

    public static String getTime(String format) {
        String date = null;
        SimpleDateFormat sdf = null;
        try {
            sdf = new SimpleDateFormat(format);
            date = sdf.format(Calendar.getInstance().getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }
    
    
    public static void main(String[] arg) {
        System.out.println(getTime("yyyyMMddHHmmss"));
    }
}
