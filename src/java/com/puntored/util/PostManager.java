
package com.puntored.util;

import com.puntored.dto.DepositImageDTO;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.apache.commons.httpclient.NameValuePair;



public class PostManager {
    
    
    static {
        disableSslVerification();
    }

    public static String sendImageToCXR(String url, String imagenBase64) {

        try {
            DepositImageDTO depositImageDTO = new DepositImageDTO();
            ObjectMapper mapper = new ObjectMapper();
//            Mcrypt Mcrypt = new Mcrypt();
//            String pswd = Mcrypt.bytesToHex(Mcrypt.encrypt(password));
            List<NameValuePair> params = new ArrayList<>();
            System.out.println("Base 64  " + imagenBase64);
            params.add(new NameValuePair("data[User][username]", "000"));//lo exige el php de app android restfull 
            params.add(new NameValuePair("data[User][password]", "000"));//lo exige el php de app android restfull 
            // los campos anteriores se envian asi poruqe el servicio web exige esos campos, en el caso de esta aplicacion no se hacen validadiocnes entonces 
            // no importa el valor que se envia
            params.add(new NameValuePair("data[image]", imagenBase64));
            params.add(new NameValuePair("data[type]", "json"));
            params.add(new NameValuePair("data[app]", "appDist"));
            System.out.println("Url! "+url);
            String respuesta = sendPost(url, getQuery(params));
            System.out.println("LA RESPUESTA " + respuesta);
            depositImageDTO = mapper.readValue(respuesta,DepositImageDTO.class);
            
            respuesta = depositImageDTO.getName();
            
            return respuesta;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    private static String getQuery(List<NameValuePair> params) {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params) {
            if (first) {
                first = false;
            } else {
                result.append("&");
            }

            try {
                result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result.toString();
    }

    public static String sendPost(String url, String query) {
        try {
            URL myurl = new URL(url);
            HttpURLConnection con = (HttpURLConnection) myurl.openConnection();

            con.setReadTimeout(50000);
            con.setConnectTimeout(15000);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-length", String.valueOf(query.length()));
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("User-Agent", "Java-Brainwinner");
            con.setDoOutput(true);
            con.setDoInput(true);

            DataOutputStream output = new DataOutputStream(con.getOutputStream());

            output.writeBytes(query);

            output.close();

            DataInputStream input = new DataInputStream(con.getInputStream());
            StringBuilder sb = new StringBuilder();

            for (int c = input.read(); c != -1; c = input.read()) {
                sb.append((char) c);

            }
            input.close();

            System.out.println("Resp Code:" + con.getResponseCode());
            System.out.println("Resp Message:" + con.getResponseMessage());

            if (con.getResponseCode() == 200) {
                return sb.toString();
            } else {
                return con.getResponseMessage();

            }
        } catch (Exception e) {
            e.printStackTrace();
            return e.getLocalizedMessage();
        }

    }
    
 
    
    
     private static void disableSslVerification() {
        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }
    
}
