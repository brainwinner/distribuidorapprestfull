/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puntored.util;

import com.puntored.dto.DistribuidorDTO;
import com.puntored.dto.EditarMargenProductoDTO;
import java.lang.reflect.Field;

/**
 *
 * @author JCruz
 */
public class ValidarCampos {

    public static boolean validString(String string, int maxLength) {
        if (string != null || !string.isEmpty()) {
            if (string.length() < maxLength && string.length() > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     *
     * @param value
     * @param maxLength
     * @return
     */
    public static boolean validInteger(Integer value, int maxLength) {
        try {
            if (value < 2147483647 && value > 0) {
//                System.out.println("Dentro del tamaño, validando el largo: " + value.toString().length());
                return value.toString().length() <= maxLength;
            } else {
//                System.out.println("Fuera del tamaño" + value);
                return false;
            }
        } catch (Exception ex) {
            System.out.println("Error al validar el valor: " + value);
            return false;
        }
    }

    public static boolean validLong(Long value, int maxLength) {
        try {
            if (value < 9223372036854775807L && value > 0) {
                return value.toString().length() <= maxLength;
            } else {
                return false;
            }
        } catch (Exception ex) {
            System.out.println("Error al validar el valor: " + value);
            return false;
        }
    }

    public static boolean validStringNumbers(String string, int maxLength) {
        boolean result = true;

        if (string != null) {
            if (!string.isEmpty()) {

                if (string.length() > 0 && string.length() <= maxLength) {
                    for (int i = 0; i < string.length(); i++) {
                        if (!Character.isDigit(string.charAt(i))) {
                            result = false;
                        }
                    }
                } else {
                    result = false;
                }
            } else {
                result = false;
            }
        } else {
            result = false;
        }
        return result;
    }

    public static boolean validIpAddress(String ip) {
        try {
            if (ip == null || ip.isEmpty()) {
                return false;
            }

            String[] parts = ip.split("\\.");
            if (parts.length != 4) {
                return false;
            }

            for (String s : parts) {
                int i = Integer.parseInt(s);
                if ((i < 0) || (i > 255)) {
                    return false;
                }
            }
            if (ip.endsWith(".")) {
                return false;
            }

            return true;
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return false;
        }
    }

    public static boolean isValidDistribuidorRequest(DistribuidorDTO dis) {
        if (dis == null) {
            System.out.println("isValidDistribuidorRequest: null Object");
            return false;
        }

        //Validar Cedula
        if (dis.getCedula() != null) {

            if (dis.getCedula().isEmpty() || dis.getCedula().equals(" ")) {
                System.out.println("isValidDistribuidorRequest: cedula vacia");
                return false;
            }

            if (!validStringNumbers(dis.getCedula(), 20)) {
                System.out.println("isValidDistribuidorRequest: cedula no cumple ocn el formato");
                return false;
            }
        }

        return true;
    }

    public static boolean inspectObject(Object o, Class<?> type) {
        Field[] fields = type.getDeclaredFields();
        boolean flag = true;
        System.out.println("-----------------------------------------");
        System.out.println("class : " + type.getSimpleName() + "  fields : " + fields.length);
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                if (field.get(o) == null) {
                    flag = false;
                    System.out.println(field.getName() + " vacio ");
                } else {
                    System.out.println(field.getName() + " - " + field.get(o).toString());
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        if (flag) {
            if (!type.getSuperclass().equals(Object.class)) {
                flag = inspectObject(o, type.getSuperclass());
            }
        }
        return flag;
    }
    
    public static boolean isValidEditMargenProducto(EditarMargenProductoDTO dto){
        
        try {
            if(dto == null){
                System.out.println("Objeto EditarMargenProductoDTO es nulo");
                return false;            
            }        
        
            DistribuidorDTO distribuidor = new DistribuidorDTO();
            distribuidor.setCedula(dto.getCedula());
            distribuidor.setClave(dto.getClave());
            if(!isValidDistribuidorRequest(distribuidor)){
                return false;
            }

            if(!validInteger(Integer.parseInt(dto.getIdComercio()), 11)){
                System.out.println("Objeto EditarMargenProductoDTO, idComercio no es valido");
                return false;
            }
            
            if(!validInteger(Integer.parseInt(dto.getIdProducto()), 11)){
                System.out.println("Objeto EditarMargenProductoDTO, idProducto no es valido");
                return false;
            }
            
            //Parseando Double para verificar
            Double margen = Double.parseDouble(dto.getMargen());
        } catch (Exception e) {
            System.out.println("Objeto EditarMargenProductoDTO no es valido, revise el log de errores");
            e.printStackTrace();
            return false;
        }
        return true;
//        
    }

}
