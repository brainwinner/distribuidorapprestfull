package com.puntored.util;

import java.util.HashMap;
import java.util.Map;


public class ResponseApi {
     
    private String state;
    private String message;
    private Object data;
    
    public static final String ERR_GENERAL_EG = "EG";
    public static final String ERR_CONN_CORE_EC = "EC";
    public static final String ERR_CONN_DB_BX = "BX";
    public static final String ERR_PARAM_INVALID_PI = "PI";
    public static final String SUCCESS_1 = "1";
    public static final String CUENTA_INVAL_2 = "2";
    public static final String PASS_INVAL_3 = "3";
    public static final String CUENTA_BLOCK_4 = "4";

    
    private Map<String, String> messages = new HashMap<>();
    
   
    public  ResponseApi () {
        
        messages.put(ERR_GENERAL_EG, "Error general");
        messages.put(ERR_CONN_CORE_EC, "Error de conexción con el core");
        messages.put(ERR_CONN_DB_BX, "Error de conexión con la base de datos");
        messages.put(ERR_PARAM_INVALID_PI, "Los parametros de entrada no son válidos");         
        messages.put(SUCCESS_1, "Solicitud efectuada con éxito.");
        messages.put(CUENTA_INVAL_2, "Cuenta invalida");
        messages.put(PASS_INVAL_3, "Clave invalida");
        messages.put(CUENTA_BLOCK_4, "Cuenta bloqueada");
  
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
        if (messages.containsKey(state)) {
            setMessage(messages.get(state));
        }   
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
