package com.puntored.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadPropertiesBD {

    static File archivo = null;
    static Properties prop = null;
    static FileInputStream fis = null;
    static ReadPropertiesBD instance = null;

    private ReadPropertiesBD() {
        openPropertiesFile();
    }

    private ReadPropertiesBD(String file) {
        openPropertiesFile(file);
    }

    public static ReadPropertiesBD getInstance() {
        if (instance == null) {
            instance = new ReadPropertiesBD();
        }
        return instance;
    }

    public static ReadPropertiesBD getInstance(String file) {
        if (instance == null) {
            instance = new ReadPropertiesBD(file);
        }
        return instance;
    }

    public static String getProperty(String key) {
        String value = null;
        value = prop.getProperty(key);
//        System.out.println(key + "=" + value);
        return value;
    }

    private static void openPropertiesFile() {
        try {
            archivo = new File("/opt/distribuidorApp/connection.properties");
//            archivo = new File("C:\\Users\\juan.salamanca\\Documents\\juan\\proyectos\\puntored\\archivos_propiedades\\connection.properties");

            prop = new Properties();
            fis = new FileInputStream(archivo);
            prop.load(fis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void openPropertiesFile(String file) {
        try {
            prop = new Properties();
            fis = new FileInputStream(file);
            prop.load(fis);
            System.out.println(prop);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
