
package com.puntored.util.sql;

import com.puntored.JDBC.BWConn;
import com.puntored.util.ResponseApi;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class SQLUtil {
    
    
    /**
     * 
     * Resive un string con la consulta del procedimiento a ejecutar y un
     * arraylist con los campos ubicados en el mismo orden que los recibe 
     * el procedimiento, el procedimeinto debe devolver los campos: 
     * idresponse, dataresponse y response
     * 
     * @param String query 
     * @param ArrayList<String> fields
     * @return 
     */
    public static ResponseApi exceuteQuery_api_response (String query, ArrayList<FieldSql> fields) {
        ResponseApi response = new ResponseApi();
        
        Connection  con = null;
        ResultSet rs = null;
        
        System.out.println("ejecutando consulta dinamica.... " + query);
        
        try {
            
            con = new BWConn().conexion();
            
            if (con != null) {
                PreparedStatement ps = con.prepareStatement(query);
                
               
                for (int i = 0; i < fields.size(); i++) {
                    FieldSql field = fields.get(i);
                    if (field.getType().equals(String.class.getSimpleName())) {
                        ps.setString( i+1, field.getValue());
                    } else if (field.getType().equals(Integer.class.getSimpleName())) {
                        ps.setInt(i+1, Integer.parseInt(field.getValue()));
                    }
                }
                rs = ps.executeQuery();
                
                if (rs.next()) {
                    int idResp = rs.getInt("idresponse");
                    
                    switch (idResp) {
                        case 1:
                            response.setData(rs.getString("dataresponse"));
                            response.setState(ResponseApi.SUCCESS_1);
                            break;
                        default:
                            response.setState(idResp+"");
                            response.setMessage(rs.getString("response"));
                            break;
                    }
                    System.out.println(" -- " + rs.getString("dataresponse"));
                } else {
                    System.err.println("No hay resultados en el result set");
                }
            } else {
              response.setState(ResponseApi.ERR_CONN_DB_BX);  
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
            response.setState(ResponseApi.ERR_GENERAL_EG);
        } finally {
            if (con != null) {
                try {
                    con.close();
                    con = null;
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return response;
    }
    
}
