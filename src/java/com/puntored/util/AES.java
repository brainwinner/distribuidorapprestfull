
package com.puntored.util;

import com.puntored.dto.AESDTO;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES {

    private static int AES_128 = 128;
    private static String ALGORITHM = "AES";
    private static String AES_CBS_PADDING = "AES/CBC/PKCS5Padding";
    
    private static final String IV = "0123456789123456";
    private static final String KEY_LAST = "puntored";

    public static byte[] encrypt(final byte[] key, final byte[] IV, final byte[] message) throws Exception {
        return encryptDecrypt(Cipher.ENCRYPT_MODE, key, IV, message);
    }

    public static byte[] decrypt(final byte[] key, final byte[] IV, final byte[] message) throws Exception {
        return encryptDecrypt(Cipher.DECRYPT_MODE, key, IV, message);
    }

    private static byte[] encryptDecrypt(final int mode, final byte[] key, final byte[] IV, final byte[] message)
            throws Exception {
        final Cipher cipher = Cipher.getInstance(AES_CBS_PADDING);
        final SecretKeySpec keySpec = new SecretKeySpec(key, ALGORITHM);
        final IvParameterSpec ivSpec = new IvParameterSpec(IV);
        cipher.init(mode, keySpec, ivSpec);
        return cipher.doFinal(message);
    }

    public static String getDataFromAESRequest(AESDTO request) {
        byte[] decryptedString = null;
        try {
//            KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHM);
//            keyGenerator.init(AES_128);

            String ks = request.getData_1().substring(0, 8) + KEY_LAST;
//            System.out.println(" key " + ks );
//            System.out.println(" Encrypt msg : " + request.getData_2());
            
            decryptedString = decrypt(ks.getBytes(), IV.getBytes(), Base64.getDecoder().decode(request.getData_2()));
//            System.out.println(" Decrypted Message : " + new String(decryptedString));
            return new String(decryptedString);
        } catch (Exception e) {
            e.printStackTrace();
            return "Error...";
        }

        
    }

}


/*
 ejemplo completo .............



            KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHM);
		keyGenerator.init(AES_128);
		//Generate Key
		SecretKey key = keyGenerator.generateKey();
		//Initialization vector
		SecretKey IV = keyGenerator.generateKey();
                
                String ks = "0123456789123456";
                String ivs = "0123456789123456";

		String randomString = "Hola bebe ";
		System.out.println("1. Message to Encrypt: " + randomString);
		
		byte[] cipherText = encrypt(ks.getBytes(), ivs.getBytes(), randomString.getBytes());
		System.out.println("2. Encrypted Text: " + Base64.getEncoder().encodeToString(cipherText));
		
		byte[] decryptedString = decrypt(ks.getBytes(), ivs.getBytes(), cipherText);
		System.out.println("3. Decrypted Message : " + new String(decryptedString));



*/