package com.puntored.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadProperties {

    static File archivo = null;
    static Properties prop = null;
    static FileInputStream fis = null;
    static ReadProperties instance = null;

    private ReadProperties() {
        openPropertiesFile();
    }

    private ReadProperties(String file) {
        openPropertiesFile(file);
    }

    public static ReadProperties getInstance() {
        if (instance == null) {
            instance = new ReadProperties();
        }
        return instance;
    }

    public static ReadProperties getInstance(String file) {
        if (instance == null) {
            instance = new ReadProperties(file);
        }
        return instance;
    }

    public static String getProperty(String key) {
        String value = null;
        value = prop.getProperty(key);
        System.out.println(key + "=" + value);
        return value;
    }

    private static void openPropertiesFile() {
        try {
            archivo = new File("/opt/core/routes.pro");
//            archivo = new File("C:\\Users\\juan.salamanca\\Documents\\juan\\proyectos\\puntored\\archivos_propiedades\\routes.pro");
//            archivo = new File("C:/opt/core/routes.pro");
//          archivo = new File("A:/routes.pro");
//          archivo = new File("C:/Users/Wilmer Alzate/Desktop/routesservinformacion.pro");
            prop = new Properties();
            fis = new FileInputStream(archivo);
            prop.load(fis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void openPropertiesFile(String file) {
        try {
            prop = new Properties();
            fis = new FileInputStream(file);
            prop.load(fis);
            System.out.println(prop);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
