
package com.puntored.util;

import com.puntored.service.client.ClientConexred;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOMsg;


public class SendISOUtil {
    
    
    public ResponseApi sendISO (HashMap<Integer, String> map) {
        ISOMsg sendISO = new ISOMsg();
        ISOMsg responseISO;
        ResponseApi response = new ResponseApi();
        ClientConexred client;
        
        System.out.println("Enviando iso.......................");
        
        try {
            
            ReadProperties comeprop = ReadProperties.getInstance();
            
            client = new ClientConexred(comeprop.getProperty("route.DistApp.host"),
                        Integer.parseInt(comeprop.getProperty("route.DistApp.port")));
            
            client.setDebug(true);
            if (client.connect()) {
                
                sendISO.setMTI("0220");
            
                for (Integer k : map.keySet()) {
                    sendISO.set(new ISOField(k, map.get(k)));
                }
                
//                sendISO.dump(System.out, "iso to send");
                
                responseISO = client.enviar(sendISO);
                
                if (responseISO != null) {
//                    responseISO.dump(System.out, "ISO resp");
                    if (responseISO.hasField(39)) {
                        if (responseISO.getString(39) != null) {
                            if (!responseISO.getString(39).equals("XX")) {
                                
                                if (responseISO.getString(39).equals("00")) {
                                    response.setState(ResponseApi.SUCCESS_1);
                                    
                                    if (responseISO.hasField(62) && responseISO.getString(62) != null) {
                                        response.setData(responseISO.getString(62));
                                    }
                                } else {
                                    System.out.println("Respuesta del core: " + responseISO.getString(39));
                                    System.out.println("Mensaje del core: " + responseISO.getString(63));
                                    response.setState(responseISO.getString(39));
                                    response.setMessage(responseISO.getString(63));
                                }
                                
                            } else {
                                System.err.println("El campo 39 es XX");
                            }
                            
                        } else {
                            System.err.println("El campo 39 es nulo");
                        }
                        
                    } else {
                        System.err.println("La respuesta no tien el campo 39");
                    }
                    
                } else {
                    System.err.println("la respuesta del core es nula");
                    response.setState(ResponseApi.ERR_CONN_CORE_EC);
                } 
                 
            } else {
                System.err.println("No se conecto con el core");
                response.setState(ResponseApi.ERR_CONN_CORE_EC);
            }   
        } catch (Exception ex) {
            ex.printStackTrace();
            response.setState(ResponseApi.ERR_GENERAL_EG);
            Logger.getLogger(SendISOUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("----------------------------finalizo........");
        return response;
    }
    
    
    
    
}
