package com.puntored.process;

import com.puntored.dto.AbonoTradicionalODTO;
import com.puntored.util.ResponseApi;
import com.puntored.util.SendISOUtil;
import com.puntored.util.ValidarCampos;
import java.util.HashMap;

public class AbonoTradicionalOnlineProcess {

    public ResponseApi getAbonoTradicionalO(AbonoTradicionalODTO dto) {
        ResponseApi response = new ResponseApi();
        System.out.println("*********************Inicio Abono tradicional Online*********************");
        System.out.println("Usuario de entrada: " + dto.getCedula());

        if (ValidarCampos.inspectObject(dto, dto.getClass())) {
            if (ValidarCampos.isValidDistribuidorRequest(dto)) {
                HashMap<Integer, String> map = new HashMap<>();

                map.put(3, "220018");
                map.put(4, dto.getValor());
                map.put(42, dto.getCedula());
                map.put(46, dto.getClave().toLowerCase());
                map.put(32, dto.getIdComercio());

                response = new SendISOUtil().sendISO(map);
            } else {
                response = new ResponseApi();
                response.setState("96");
            }
        } else {
            System.out.println("Los datos no estan completos");
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }

        System.out.println("********************* finalizo Abono tradicional Online*********************");
        return response;

    }

}
