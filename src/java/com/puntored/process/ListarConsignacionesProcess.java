package com.puntored.process;

import com.puntored.JDBC.BWConn;
import com.puntored.dto.DistribuidorDTO;
import com.puntored.util.ResponseApi;
import com.puntored.util.ValidarCampos;
import com.puntored.util.sql.FieldSql;
import com.puntored.util.sql.SQLUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ListarConsignacionesProcess {

    public ResponseApi getConsignaciones(DistribuidorDTO request) {

        ResponseApi response = null;

        System.out.println("*********************Listar Consignaciones*********************");
        System.out.println("Usuario de entrada: " + request.getCedula());

        if (ValidarCampos.isValidDistribuidorRequest(request)) {
            ArrayList<FieldSql> fields = new ArrayList<>();

            fields.add(new FieldSql(String.class.getSimpleName(), request.getCedula()));
            fields.add(new FieldSql(String.class.getSimpleName(), request.getClave()));

            response = SQLUtil.exceuteQuery_api_response("select * from \"APPDISTRIBUIDOR\".listar_consignaciones_web(?,?)", fields);

        } else {
            response = new ResponseApi();
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }

        System.out.println("*********************Fin Listar Consignaciones*********************");
        return response;
    }

}
