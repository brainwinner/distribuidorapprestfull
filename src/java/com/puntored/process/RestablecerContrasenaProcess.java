/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puntored.process;

import com.puntored.dto.DistribuidorDTO;
import com.puntored.dto.RestablecerClaveDTO;

import com.puntored.service.client.ClientConexred;
import com.puntored.util.ReadProperties;
import com.puntored.util.ResponseApi;
import com.puntored.util.SendISOUtil;
import com.puntored.util.ValidarCampos;
import java.util.HashMap;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOMsg;

/**
 *
 * @author JCruz
 */
public class RestablecerContrasenaProcess {

    public ResponseApi restablecerClave(RestablecerClaveDTO dto) {
        System.out.println("realizando login...");
        ResponseApi response = null;
        
        if (dto.getIdDistribuidor() != null && dto.getClave() != null) {
            HashMap<Integer, String> map = new HashMap<>();
            map.put(3, "220013");
            map.put(46, dto.getIdDistribuidor().trim());
            map.put(47, dto.getClave().trim());
            response = new SendISOUtil().sendISO(map);
        } else {
            response = new ResponseApi();
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }
        return response;
    }

}
