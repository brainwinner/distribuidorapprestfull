/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puntored.process;

import com.puntored.dto.AceptarRechazarAbonoDTO;
import com.puntored.service.client.ClientConexred;
import com.puntored.util.ReadProperties;
import com.puntored.util.ResponseApi;
import com.puntored.util.SendISOUtil;
import com.puntored.util.ValidarCampos;
import java.util.HashMap;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOMsg;

/**
 *
 * @author JCruz
 */
public class RechazarAbonoProcess {

    public ResponseApi rechazarAbono(AceptarRechazarAbonoDTO dto) {
        ResponseApi response = new ResponseApi();

        System.out.println("*********************Rechazar Abono*********************");
        System.out.println("Usuario de entrada: " + dto.getCedula());
        HashMap<Integer, String> map = new HashMap<>();
        
        if (ValidarCampos.inspectObject(dto, dto.getClass())) {
            map.put(3, "220022");
            map.put(42, dto.getCedula());
            map.put(46, dto.getClave().toLowerCase());
            map.put(32, dto.getIdAbono());
            response = new SendISOUtil().sendISO(map);
 
        } else {
            System.out.println("Los datos no estan completos");
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }
        

        return response;
    }

}
