
package com.puntored.process;


import com.puntored.dto.ListarCiudadesDTO;
import com.puntored.util.ResponseApi;
import com.puntored.util.sql.SQLUtil;
import com.puntored.util.ValidarCampos;
import com.puntored.util.sql.FieldSql;
import java.util.ArrayList;




public class ListarCiudadesProcess {
    
    public ResponseApi getCiudades(ListarCiudadesDTO request) {
        ResponseApi response = null;
   
        if (ValidarCampos.isValidDistribuidorRequest(request)) {
            if (request.getIdDepartamento() != null) {
                ArrayList<FieldSql> fields = new ArrayList<>();
        
                
                fields.add(new FieldSql(String.class.getSimpleName(), request.getCedula()));
                fields.add(new FieldSql(String.class.getSimpleName(), request.getClave()));
                fields.add(new FieldSql(String.class.getSimpleName(), request.getIdDepartamento()));
                

                response = SQLUtil.exceuteQuery_api_response("select * from \"APPDISTRIBUIDOR\".consultar_ciudades(?,?,?)", fields);
    
            } else {
                response = new ResponseApi();
                response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
            }
        } else {
            response = new ResponseApi();
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }
        
        return response;
                
    } 
    
    
}


      
//        
//        try {
//            
//            if (true) {
//                con = new BWConn().conexion();
//
//                PreparedStatement ps = con.prepareStatement("select * from \"APPDISTRIBUIDOR\".consultar_ciudades(?,?,?)");
//                ps.setString(1, request.getCedula());
//                ps.setString(2, request.getClave());
//                ps.setString(3, request.getIdDepartamento());
//
//                rs = ps.executeQuery();
//                
//                if (rs.next()) {
//                    
//                    int idResp = rs.getInt("idresponse");
//                    
//                    switch (idResp) {
//                        case 1:
//                            response.setData(rs.getString("dataresponse"));
//                            response.setState("1");
//                            break;
//                        default:
//                            response.setState(idResp+"");
//                            response.setMessage(rs.getString("response"));
//                            break;
//                    }
//                    
//                    System.out.println(" -- " + rs.getString("dataresponse"));
//                } else {
//                    response.setState("2");
//                }
//                
//            } else {
//                response.setState("5");
//            }
//            
//            
//            
//            
//            
//            
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (con != null) {
//                try {
//                    con.close();
//                    con = null;
//                } catch (SQLException ex) {
//                    ex.printStackTrace();
//                }
//            }
//        }
//        
        
      