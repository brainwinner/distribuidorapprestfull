
package com.puntored.process;

import com.puntored.dto.EditarMargenProductoDTO;
import com.puntored.util.ResponseApi;
import com.puntored.util.SendISOUtil;
import com.puntored.util.ValidarCampos;
import java.util.HashMap;


public class EditarMargenProductosProcess {
    
    public ResponseApi editMargenProducto (EditarMargenProductoDTO dto) {
        ResponseApi response = new ResponseApi();
        System.out.println("*********************Inicio Editar Margen Productos*********************");
        System.out.println("Usuario de entrada: " + dto.getCedula());
        
        if (ValidarCampos.isValidEditMargenProducto(dto)) {
            HashMap<Integer, String> map = new HashMap<>();

            map.put(3, "220025");
            map.put(42, dto.getCedula());
            map.put(46, dto.getClave().toLowerCase());
            map.put(32, dto.getIdComercio());
            map.put(82, dto.getIdProducto());
            map.put(83, dto.getMargen());

           response = new SendISOUtil().sendISO(map);
        } else {
            response = new ResponseApi();
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }
        System.out.println("********************* finalizo Editar Margen Productos*********************");
        return response;
    }
    
}
