package com.puntored.process;

import com.puntored.dto.LoginTerminalDTO;
import com.puntored.service.client.ClientConexred;
import com.puntored.util.ReadProperties;
import com.puntored.util.ResponseApi;
import com.puntored.util.SendISOUtil;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOMsg;

/**
 *
 * @author Holding Digital
 */
public class LoginTerminalProcess {
    
    public ResponseApi login(LoginTerminalDTO request) {
        System.out.println("realizando login...");
        
        ResponseApi response;
        
        HashMap<Integer, String> map = new HashMap<>();

        map.put(3, "910222");
        map.put(41, request.getTerminalId());
        map.put(43, request.getClave().toLowerCase());

        response = sendISO(map);
        
        return response;
    }
    
    private ResponseApi sendISO (HashMap<Integer, String> map) {
        ISOMsg sendISO = new ISOMsg();
        ISOMsg responseISO;
        ResponseApi response = new ResponseApi();
        ClientConexred client;
        
        System.out.println("Enviando iso.......................");
        
        try {
            ReadProperties comeprop = ReadProperties.getInstance();
            
            client = new ClientConexred(comeprop.getProperty("route.DistApp.host"), Integer.parseInt(comeprop.getProperty("route.DistApp.port")));
            client.setDebug(false);
            
            if (client.connect()) {
                sendISO.setMTI("0220");
            
                for (Integer k : map.keySet()) {
                    sendISO.set(new ISOField(k, map.get(k)));
                }
                
                responseISO = client.enviar(sendISO);
                
                if (responseISO != null) {
                    if (responseISO.hasField(39)) {
                        if (responseISO.getString(39) != null) {
                            if (responseISO.getString(39).equals("00")) {
                                response.setState(ResponseApi.SUCCESS_1);

                                if (responseISO.hasField(63) && responseISO.getString(63) != null) {
                                    response.setData(responseISO.getString(63));
                                }
                            } else {
                                System.out.println("Respuesta del core: " + responseISO.getString(39));
                                System.out.println("Mensaje del core: " + responseISO.getString(63));
                                response.setState(responseISO.getString(39));
                                response.setMessage(responseISO.getString(63));
                            }
                        } else {
                            System.err.println("El campo 39 es nulo");
                        }
                    } else {
                        System.err.println("La respuesta no tien el campo 39");
                    }
                } else {
                    System.err.println("la respuesta del core es nula");
                    response.setState(ResponseApi.ERR_CONN_CORE_EC);
                } 
            } else {
                System.err.println("No se conecto con el core");
                response.setState(ResponseApi.ERR_CONN_CORE_EC);
            }   
        } catch (Exception ex) {
            ex.printStackTrace();
            response.setState(ResponseApi.ERR_GENERAL_EG);
            Logger.getLogger(SendISOUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("----------------------------finalizo........");
        return response;
    }
}
