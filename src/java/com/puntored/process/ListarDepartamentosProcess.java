
package com.puntored.process;


import com.puntored.dto.DistribuidorDTO;
import com.puntored.util.ResponseApi;
import com.puntored.util.sql.SQLUtil;
import com.puntored.util.ValidarCampos;
import com.puntored.util.sql.FieldSql;
import java.util.ArrayList;



public class ListarDepartamentosProcess {
    
    
    public ResponseApi getDepartamentos (DistribuidorDTO request) {
        ResponseApi response = null;
        
        if (ValidarCampos.isValidDistribuidorRequest(request)) {
            ArrayList<FieldSql> fields = new ArrayList<>();
        
            fields.add(new FieldSql(String.class.getSimpleName(), request.getCedula()));
            fields.add(new FieldSql(String.class.getSimpleName(), request.getClave()));
            

            response = SQLUtil.exceuteQuery_api_response("select * from \"APPDISTRIBUIDOR\".consultar_departamentos(?,?)", fields);
        
        } else {
            response = new ResponseApi();
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }
        
           
        
        
        return response;
    }
    
   
    
}


      
//        
//        ResultSet rs = null;
//        Connection con = null;
//        
//        try {
//            if (isValidInfoDistRequest(request)) {
//                
//                con = new BWConn().conexion();
//
//                PreparedStatement ps = con.prepareStatement("select * from \"APPDISTRIBUIDOR\".consultar_departamentos(?,?)");
//                ps.setString(1, request.getCedula());
//                ps.setString(2, request.getClave());
//
//                rs = ps.executeQuery();
//                
//                if (rs.next()) {
//                    
//                    int idResp = rs.getInt("idresponse");
//                    
//                    switch (idResp) {
//                        case 1:
//                            response.setData(rs.getString("dataresponse"));
//                            response.setState("1");
//                            break;
//                        default:
//                            response.setState(idResp+"");
//                            response.setMessage(rs.getString("response"));
//                            break;
//                    }
//                    
//                    System.out.println(" -- " + rs.getString("dataresponse"));
//                } else {
//                    response.setState("2");
//                }
//                rs.close();
//                ps.close();
//                
//                
//                
//            } else {
//                response.setState("2");
//            }
//            
//            
//            
//        } catch (Exception e) {
//            e.printStackTrace();
//            response.setMessage(e.getMessage());
//            response.setState("3");
//        } finally {
//            if (con != null) {
//                try {
//                    con.close();
//                    con = null;
//                } catch (SQLException ex) {
//                    ex.printStackTrace();
//                }
//            }
//        }
//         