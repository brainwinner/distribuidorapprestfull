
package com.puntored.process;

import com.puntored.dto.ListarCausalNoPagoDTO;
import com.puntored.util.ResponseApi;
import com.puntored.util.ValidarCampos;
import com.puntored.util.sql.FieldSql;
import com.puntored.util.sql.SQLUtil;
import java.util.ArrayList;

public class ListarCausalNoPagoProcess {
    
    public ResponseApi getCausalNoPago(ListarCausalNoPagoDTO dto) {
        
        ResponseApi response = null;

        System.out.println("*********************Listar Causal no pago*********************");
        System.out.println("Usuario de entrada: " + dto.getCedula());

        if (ValidarCampos.isValidDistribuidorRequest(dto)) {
            ArrayList<FieldSql> fields = new ArrayList<>();

            fields.add(new FieldSql(String.class.getSimpleName(), dto.getCedula()));
            fields.add(new FieldSql(String.class.getSimpleName(), dto.getClave()));
            // si el tipo es 1 se consultan los de bloqueo si es 2 los de desbloqueo
            fields.add(new FieldSql(Integer.class.getSimpleName(), dto.getTipo()));

            response = SQLUtil.exceuteQuery_api_response("select * from \"APPDISTRIBUIDOR\".consultar_causales_no_pago(?,?,?)", fields);

        } else {
            response = new ResponseApi();
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }

        System.out.println("*********************Fin Listar Causal no pago*********************");
        return response;
    }
}
