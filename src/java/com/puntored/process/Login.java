/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puntored.process;

import com.puntored.dto.DistribuidorDTO;
import com.puntored.util.ResponseApi;
import com.puntored.util.SendISOUtil;
import com.puntored.util.ValidarCampos;
import java.util.HashMap;

/**
 *
 * @author JCruz
 */
public class Login {

    public ResponseApi makeLogin(DistribuidorDTO request) {
        System.out.println("realizando login...");
        ResponseApi response = null ;
        if (ValidarCampos.isValidDistribuidorRequest(request)) {
            HashMap<Integer, String> map = new HashMap<>();

            map.put(3, "220011");
            map.put(42, request.getCedula());
            map.put(46, request.getClave().toLowerCase());

           response = new SendISOUtil().sendISO(map);
        } else {
            response = new ResponseApi();
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }
        
        
        return response;
    }
}
