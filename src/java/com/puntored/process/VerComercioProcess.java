
package com.puntored.process;

import com.puntored.dto.VerComercioDTO;
import com.puntored.util.ResponseApi;
import com.puntored.util.ValidarCampos;
import com.puntored.util.sql.FieldSql;
import com.puntored.util.sql.SQLUtil;
import java.util.ArrayList;


public class VerComercioProcess {
    
    
    public ResponseApi getComercio (VerComercioDTO dto) {
        
        ResponseApi response = null;

        System.out.println("*********************Inicio Ver comercio*********************");
        System.out.println("Usuario de entrada: " + dto.getCedula());

        if (ValidarCampos.isValidDistribuidorRequest(dto)) {
            ArrayList<FieldSql> fields = new ArrayList<>();

            fields.add(new FieldSql(String.class.getSimpleName(), dto.getCedula()));
            fields.add(new FieldSql(String.class.getSimpleName(), dto.getClave()));
            fields.add(new FieldSql(Integer.class.getSimpleName(), dto.getIdComercio()));

            response = SQLUtil.exceuteQuery_api_response("select * from \"APPDISTRIBUIDOR\".ver_comercio(?,?,?)", fields);

        } else {
            response = new ResponseApi();
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }

        System.out.println("*********************Fin ver comercio*********************");
        return response;
    }
    
}
