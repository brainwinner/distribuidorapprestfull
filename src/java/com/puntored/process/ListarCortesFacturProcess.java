
package com.puntored.process;

import com.puntored.dto.DistribuidorDTO;
import com.puntored.util.ResponseApi;
import com.puntored.util.ValidarCampos;
import com.puntored.util.sql.FieldSql;
import com.puntored.util.sql.SQLUtil;
import java.util.ArrayList;


public class ListarCortesFacturProcess {
    
    public ResponseApi getCortesFactr(DistribuidorDTO dto) {
        
        ResponseApi response = null;

        System.out.println("*********************Listar Cortes Facturacion*********************");
        System.out.println("Usuario de entrada: " + dto.getCedula());

        if (ValidarCampos.isValidDistribuidorRequest(dto)) {
            ArrayList<FieldSql> fields = new ArrayList<>();

            fields.add(new FieldSql(String.class.getSimpleName(), dto.getCedula()));
            fields.add(new FieldSql(String.class.getSimpleName(), dto.getClave()));

            response = SQLUtil.exceuteQuery_api_response("select * from \"APPDISTRIBUIDOR\".consultar_cortes_facturacion(?,?)", fields);

        } else {
            response = new ResponseApi();
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }

        System.out.println("*********************Fin Listar Cortes Facturacion*********************");
        return response;
        
    } 
                
    
}
