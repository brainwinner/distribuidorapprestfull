/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puntored.process;

import com.puntored.dto.AfiliacionDTO;
import com.puntored.util.ResponseApi;
import com.puntored.util.SendISOUtil;
import java.util.HashMap;

/**
 *
 * @author JCruz
 */
public class AfiliacionProcess {

    public ResponseApi makeAfiliacion(AfiliacionDTO request) {
        System.out.println("realizando login...");
        ResponseApi response;   
        HashMap<Integer, String> map = new HashMap<>();
        System.out.println("Data json: "+request.getJsonData());
        map.put(3, "220024");
        String jsonString = request.getJsonData();
        if (jsonString.length() > 999) {
            String firstString = jsonString.substring(0, 999);
            String secondString = jsonString.substring(999, jsonString.length());
            map.put(105, firstString);
            map.put(106, secondString);
        } else {
            map.put(105, jsonString);
        }
        response = new SendISOUtil().sendISO(map);
        return response;
    }
}
