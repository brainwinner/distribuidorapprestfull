
package com.puntored.process;

import com.puntored.dto.DistribuidorDTO;
import com.puntored.dto.ListarAbonosDTO;
import com.puntored.util.ResponseApi;
import com.puntored.util.sql.SQLUtil;
import com.puntored.util.ValidarCampos;
import com.puntored.util.sql.FieldSql;
import java.util.ArrayList;


public class ListarAbonos {
    
    public ResponseApi getAbonos (ListarAbonosDTO request) {
        ResponseApi response = null;
        
        System.out.println("*********************Listar Abonos*********************");
        System.out.println("Usuario de entrada: " + request.getCedula());
        
        
        if (ValidarCampos.isValidDistribuidorRequest(request)) {
            ArrayList<FieldSql> fields = new ArrayList<>();
        
            fields.add(new FieldSql(String.class.getSimpleName(), request.getCedula()));
            fields.add(new FieldSql(String.class.getSimpleName(), request.getClave()));
            fields.add(new FieldSql(Integer.class.getSimpleName(), request.getIdComercio()));
            


            response = SQLUtil.exceuteQuery_api_response("select * from \"APPDISTRIBUIDOR\".listar_abonos(?,?,?)", fields);
        
        } else {
            response = new ResponseApi();
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }
        
        
        System.out.println("*********************Fin Listar Abonos*********************");
        
        
        
        return response;
    }
    
}
