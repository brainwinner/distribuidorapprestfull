/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puntored.process;

import com.puntored.JDBC.BWConn;
import com.puntored.dto.DistribuidorDTO;
import com.puntored.util.ResponseApi;
import com.puntored.util.sql.SQLUtil;
import com.puntored.util.ValidarCampos;
import static com.puntored.util.ValidarCampos.isValidDistribuidorRequest;
import com.puntored.util.sql.FieldSql;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListarComerciosProcess {

    public ResponseApi getComercios(DistribuidorDTO request) {
       
        ResponseApi response = null;
        System.out.println("*********************Listar Comercions*********************");
        System.out.println("Usuario de entrada: " + request.getCedula());

        if (ValidarCampos.isValidDistribuidorRequest(request)) {
            ArrayList<FieldSql> fields = new ArrayList<>();
            
            fields.add(new FieldSql(String.class.getSimpleName(), request.getCedula()));
            fields.add(new FieldSql(String.class.getSimpleName(), request.getClave()));
        
            response = SQLUtil.exceuteQuery_api_response("select * from \"APPDISTRIBUIDOR\".consultar_comercios(?,?)", fields);

        } else {
            response = new ResponseApi();
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }
        
        
        System.out.println("*********************Fin Listar Comercio*********************");
        return response;
    }
}



//        try {
//
//            if (isValidDistribuidorRequest(request)) {
//                con = new BWConn().conexion();
//                
//                if(con == null){
//                    System.out.println("Sin conexion a la base de datos");
//                    response.setState("6");
//                    return response;
//                }
//
//                PreparedStatement ps = con.prepareStatement("select * from \"APPDISTRIBUIDOR\".consultar_comercios(?,?)");
//                ps.setString(1, request.getCedula());
//                ps.setString(2, request.getClave());
//
//                System.out.println("Ejecutando procedimiento almacenado.....");
//                rs = ps.executeQuery();
//
//                if (rs.next()) {
//                    
//                    int state = rs.getInt("idresponse");
//                    System.out.println("State: "+ state);
//                    System.out.println("Response: "+ rs.getString("response"));
//                    switch (state) {
//                        case 1:
//                            response.setData(rs.getString("dataresponse"));
//                            response.setState("1");
//                            break;
//                        case 2:
//                        case 3:
//                            response.setData(rs.getString("dataresponse"));
//                            response.setState("2"); 
//                            break;
//                        case 4:
//                            response.setData(rs.getString("dataresponse"));
//                            response.setState("3");
//                            break;
//                         default:
//                            response.setState(state+"".trim());
//                            response.setMessage(rs.getString("response"));
//                            break;
//                    }
//                } else {
//                    System.out.println("Sin respuesta del procedimiento");
//                    response.setState("4");
//                }
//                rs.close();
//                ps.close();
//
//            } else {
//                System.out.println("Parametros de entrada nom cumplen con el formato");
//                response.setState("5");  
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            response.setState("4");
//        } finally {
//            if (con != null) {
//                try {
//                    con.close();
//                    con = null;
//                } catch (SQLException ex) {
//                    ex.printStackTrace();
//                    Logger.getLogger(InfoDistribuidorProcess.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
