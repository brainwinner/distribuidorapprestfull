/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puntored.process;

import com.puntored.dto.AceptarRechazarAbonoDTO;
import com.puntored.dto.PasaCupoDTO;
import com.puntored.util.ResponseApi;
import com.puntored.util.SendISOUtil;
import com.puntored.util.ValidarCampos;
import java.util.HashMap;


public class PasarCupoProcess {
      public ResponseApi pasarCupo(PasaCupoDTO dto) {
        ResponseApi response = new ResponseApi();

        System.out.println("*********************Pasar Cupo*********************");
        System.out.println("Usuario de entrada: " + dto.getCedula());
        HashMap<Integer, String> map = new HashMap<>();
        
        if (ValidarCampos.inspectObject(dto, dto.getClass())) {
            map.put(3, "220023");
            map.put(4, dto.getMonto());
            map.put(42, dto.getCedula());
            map.put(46, dto.getClave().toLowerCase());
            map.put(32, dto.getIdComercio());
            response = new SendISOUtil().sendISO(map);
 
        } else {
            System.out.println("Los datos no estan completos");
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }
        

        return response;
    }
    
}
