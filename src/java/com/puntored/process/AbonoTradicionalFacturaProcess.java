
package com.puntored.process;

import com.puntored.dto.AbonoTradicionalFDTO;
import com.puntored.util.ResponseApi;
import com.puntored.util.SendISOUtil;
import com.puntored.util.ValidarCampos;
import java.util.HashMap;


public class AbonoTradicionalFacturaProcess {
    
    public ResponseApi getAbonoTradicional (AbonoTradicionalFDTO dto) {
        ResponseApi response = new ResponseApi();
        System.out.println("*********************Inicio Abono tradicional factura*********************");
        System.out.println("Usuario de entrada: " + dto.getCedula());
        
        if (ValidarCampos.isValidDistribuidorRequest(dto)) {
            HashMap<Integer, String> map = new HashMap<>();

            map.put(3, "220016");
            map.put(42, dto.getCedula());
            map.put(46, dto.getClave().toLowerCase());
            map.put(32, dto.getIdComercio());

           response = new SendISOUtil().sendISO(map);
        } else {
            response = new ResponseApi();
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }
        System.out.println("********************* finalizo Abono tradicional factura*********************");
        return response;
    }
    
}
