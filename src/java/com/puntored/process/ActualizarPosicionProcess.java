
package com.puntored.process;

import com.puntored.dto.ActualizarPosicionDTO;
import com.puntored.util.ResponseApi;
import com.puntored.util.SendISOUtil;
import com.puntored.util.ValidarCampos;
import java.util.HashMap;

public class ActualizarPosicionProcess {
    
    
    public ResponseApi getActualizarPosicion (ActualizarPosicionDTO dto) {
        ResponseApi response = new ResponseApi();
        System.out.println("*********************Inicio ActualizarPosicion*********************");
        System.out.println("Usuario de entrada: " + dto.getCedula());
        
        if (ValidarCampos.isValidDistribuidorRequest(dto)) {
            HashMap<Integer, String> map = new HashMap<>();

            map.put(3, "220021");
            map.put(42, dto.getCedula());
            map.put(46, dto.getClave().toLowerCase());
            map.put(32, dto.getIdComercio());
            map.put(102, dto.getLatitud());
            map.put(103, dto.getLongitud());

           response = new SendISOUtil().sendISO(map);
        } else {
            response = new ResponseApi();
            response.setState(ResponseApi.ERR_PARAM_INVALID_PI);
        }
        System.out.println("********************* finalizo ActualizarPosicion*********************");
        return response;
    }
    
}
