package com.puntored.process;

import com.puntored.dto.CargarConsignacionDTO;
import com.puntored.util.PostManager;
import com.puntored.util.ReadPropertiesBD;

import com.puntored.util.ResponseApi;
import com.puntored.util.SendISOUtil;
import com.puntored.util.TimeUtil;
import com.puntored.util.ValidarCampos;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import sun.misc.BASE64Decoder;

public class CargarConsignacionProcess {

    public ResponseApi cargarConsignacion(CargarConsignacionDTO dto) {

        ResponseApi response = new ResponseApi();

        System.out.println("*********************Cargar Consignacion*********************");
        System.out.println("Usuario de entrada: " + dto.getCedula());
        HashMap<Integer, String> map = new HashMap<>();
        
        
        if (ValidarCampos.inspectObject(dto, dto.getClass())) {
            ReadPropertiesBD prop = ReadPropertiesBD.getInstance();
            String hostCXR = prop.getProperty("puntoadmin.host");
            String rutaConsignacion = prop.getProperty("puntoadmin.carpeta_consignaciones");
            String url = hostCXR + rutaConsignacion;

            String urlImage = PostManager.sendImageToCXR(url, dto.getImagen().split(",")[1]);           
            System.out.println("result sxr: " + urlImage);
       
            if (urlImage != null && !urlImage.equals("")) {
                map.put(3, "220014");
                map.put(42, dto.getCedula());
                map.put(46, dto.getClave().toLowerCase());
                map.put(104, dto.getFechaConsignacion());
                map.put(82, dto.getIdDistribuidor());
                map.put(83, dto.getIdBanco());
                map.put(44, dto.getIdDepartamento());
                map.put(37, dto.getIdCiudad());
                map.put(125, dto.getSucursal());
                map.put(4, dto.getValor());
                map.put(114, urlImage); //imagen
                map.put(124, dto.getNumeroConsignacion());

                response = new SendISOUtil().sendISO(map);
            } else {
                System.out.println("No se pudo guardar la imagen");
                response.setState("96");
            }
        } else {
            System.out.println("Los datos no estan completos");
            response.setState("96");
        }

        System.out.println("*********************Fin Cargar Consignacion*********************");
        return response;
    }

    

    private String base64ToPath(CargarConsignacionDTO dto) {
        String path = null;
        String route;
        String imgName;
        try {

            String[] parts = dto.getImagen().split(",");
            String imageString = parts[1];

            BufferedImage image = null;
            byte[] imageByte;

            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();

//            route = "C:\\opt\\distribuidorApp\\";
//            route = "/opt/distribuidorApp/";
//            route = "/home/jsalamanca/";
            route = "192.168.3.27/cxrsys/tirilla/images/";
            imgName = "consig_" + dto.getIdBanco() + "_"
                    + dto.getNumeroConsignacion() + "_"
                    + TimeUtil.getTime("yyyyMMddHHmmss") + ".jpg";
            path = route + imgName;

            // write the image to a file
            File outputfile = new File(path);
            ImageIO.write(image, "jpg", outputfile);
            
            File tFile = new File(path);
            
            
            
            if (tFile.isFile()) {
                System.out.println("el fichero exite... " + tFile.getName());
                
                if (tFile.canRead()) {
                    System.out.println("el archivos e puede leer");
                } else {
                    System.out.println("no se puede leer");
                }
                
                
            } else {
                System.out.println("El archivo no exite .......*****/*");
            }
            

        } catch (Exception ex) {
            ex.printStackTrace();
            path = null;
            Logger.getLogger(CargarConsignacionProcess.class.getName()).log(Level.SEVERE, null, ex);
        }

        return path;

    }


}
