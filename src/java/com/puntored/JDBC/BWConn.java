/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puntored.JDBC;

import com.puntored.util.ReadPropertiesBD;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author JCruz
 */
public class BWConn {

    public Connection conexion(){
        Connection conn = null;
        
        ReadPropertiesBD readProp = ReadPropertiesBD.getInstance();
        
        String user = readProp.getProperty("user");
        String password = readProp.getProperty("password");
        String bd = readProp.getProperty("bd");
        String host = readProp.getProperty("host");
        String port = readProp.getProperty("port");
            
        if(user == null || password == null || bd == null || host == null || port == null){
            System.out.println("Error al leer archuvo de propiedades SQL");
            return conn;
        }
        
        String url = "jdbc:postgresql://"+host+":"+port+""+"/"+bd;
        try{
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(url, user, password);
        }catch(ClassNotFoundException | SQLException e){
            System.out.println(e);
        }
        return conn;
    }        
    
    
    
}
